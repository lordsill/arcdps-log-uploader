﻿using System;
using System.IO;
using System.Text.Json;
using ArcDPS_Log_Uploader.Entity;

namespace ArcDPS_Log_Uploader
{
    internal static class Program
    {
        internal static string Storage { get; private set; }
        internal static Configuration Config { get; private set; }

        internal const string Header =
            "###############################\n##### ArcDPS Log Uploader #####\n###### by lordsill.3574 #######\n###############################\n";

        public static void Main(string[] args)
        {
            Console.Title = "ArcDPS Log Uploader";
            Console.Clear();
            Console.WriteLine(Header);
            if (Setup.IsFirstStart())
                Setup.Run();
            Config = JsonSerializer.Deserialize<Configuration>(File.ReadAllText("config.json"));
            var ic = new InteractiveConsole.InteractiveConsole();
            var menu = ic.CreateSingleSelectionMenu(Header + "\nPlease select the Collection you want to use:");
            foreach (var storage in Config.Storages)
            {
                menu.AddEntry(storage);
            }
            menu.AddEntry("Add Collection");
            var elem = ic.Draw()[0];
            if (elem.Index == Config.Storages.Length)
            {
                AddCollection.Run();
                return;
            }

            Storage = elem.Text;
            Console.Clear();
            Console.WriteLine(Header);
            LogDirWatcher.Run();

            while (true)
                Console.ReadKey(true);
        }
    }
}