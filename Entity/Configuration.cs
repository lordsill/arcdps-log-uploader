﻿namespace ArcDPS_Log_Uploader.Entity
{
    public class Configuration
    {
        public string DpsReportKey { get; set; }
        public string UploadKey { get; set; }
        public string[] Storages { get; set; }
    }
}