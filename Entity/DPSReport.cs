﻿using ArcDPS_Log_Uploader.Entity.Report;

namespace ArcDPS_Log_Uploader.Entity
{
    public class DPSReport
    {
        public string id { get; init; }
        public string link { get; init; }
        public bool cm { get; init; }
        public bool success { get; init; }
        public Player[] players { get; init; }
        public Time time { get; init; }
        public Boss[] boss { get; init; }
    }
}