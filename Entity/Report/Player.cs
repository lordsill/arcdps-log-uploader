﻿namespace ArcDPS_Log_Uploader.Entity.Report
{
    public class Player
    {
        public string account { get; init; }
        public string character { get; init; }
        public int profession { get; init; }
        public int specialisation { get; init; }
        public int subgroup { get; init; }
    }
}