﻿namespace ArcDPS_Log_Uploader.Entity.Report
{
    public class Time
    {
        public long start { get; init; }
        public long end { get; init; }
        public int duration { get; init; }
    }
}