﻿namespace ArcDPS_Log_Uploader.Entity
{
    public class APIUploadResponse
    {
        public bool success { get; init; }
        public string error { get; init; }
        public DPSReport data { get; init; }
    }
}