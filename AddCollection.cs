﻿using System;
using System.IO;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace ArcDPS_Log_Uploader
{
    public static class AddCollection
    {
        internal static void Run()
        {
            Console.Clear();
            Console.WriteLine(Program.Header);
            Console.WriteLine("Please enter the name of the collection:");
            var name = Console.ReadLine();
            if (name?.Length > 0)
            {
                var regex = new Regex(@"^[a-z0-9_]+$");
                if (!regex.Match(name).Success)
                {
                    Console.WriteLine("Invalid name of collection.");
                    Console.ReadKey(true);
                    Console.Clear();
                    Program.Main(Array.Empty<string>());
                    return;
                }
                var storages = Program.Config.Storages;
                Array.Resize(ref storages, storages.Length + 1);
                storages[^1] = name;
                Program.Config.Storages = storages;
                File.WriteAllText("config.json", JsonSerializer.Serialize(Program.Config));
            }
            Console.Clear();
            Program.Main(Array.Empty<string>());
        }
    }
}