﻿using System.IO;
using System.Net.Http;
using System.Text.Json;
using ArcDPS_Log_Uploader.Entity;

namespace ArcDPS_Log_Uploader
{
    public class Client
    {
        private const string BaseUrl = "https://tools-api.lordsill.de/gw2/logs";

        internal static APIUploadResponse Upload(string storage, string path) => Upload(storage, null, path);

        internal static APIUploadResponse Upload(string storage, string token, string path)
        {
            if (storage == null || Program.Config.UploadKey == null) return null;
            using var client = new HttpClient();
            using var form = new MultipartFormDataContent();
            var fileContent = File.ReadAllBytes(path);
            form.Add(new ByteArrayContent(fileContent, 0, fileContent.Length), "file", Path.GetFileName(path));
            if (token != null)
            {
                form.Add(new StringContent(token), "token");
            }

            form.Add(new StringContent(storage), "collection");
            form.Add(new StringContent(Program.Config.UploadKey), "key");
            var result = client.PostAsync(BaseUrl + "/upload", form).Result;
            return result.IsSuccessStatusCode
                ? JsonSerializer.Deserialize<APIUploadResponse>(result.Content.ReadAsStringAsync().Result)
                : null;
        }
    }
}