﻿using System;

namespace ArcDPS_Log_Uploader.InteractiveConsole
{
    public class ICMultiSelectionMenu : ICMenu
    {
        private readonly string _title;

        internal ICMultiSelectionMenu(string title) => this._title = title;

        internal override ICMenuElement Select()
        {
            if (MenuIndex == MenuEntries.Count - 1)
            {
                IsDone = true;
                return null;
            }
            var element = MenuEntries[MenuIndex];
            element.IsSelected = !element.IsSelected;
            Display();
            return element;
        }

        internal override void UIFinalize()
        {
            AddEntry("Done");
            base.UIFinalize();
        }

        internal override void Display()
        {
            Console.Clear();
            Console.WriteLine(this._title);
            for (var i = 0; i < MenuEntries.Count; i++)
            {
                Console.Write(i == MenuIndex ? "> " : "  ");

                if(i != MenuEntries.Count - 1 && MenuEntries[i].IsSelected) Console.Write("[X] ");
                else if(i != MenuEntries.Count - 1) Console.Write("[ ] ");
                
                Console.WriteLine(MenuEntries[i].Text);
            }
        }
    }
}