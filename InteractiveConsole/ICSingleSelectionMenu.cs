﻿namespace ArcDPS_Log_Uploader.InteractiveConsole
{
    public class ICSingleSelectionMenu : ICMenu
    {
        private readonly string _title;

        internal ICSingleSelectionMenu(string title)
        {
            this._title = title;
        }

        internal override ICMenuElement Select()
        {
            var element = MenuEntries[MenuIndex];
            element.IsSelected = true;
            IsDone = true;
            return element;
        }

        internal override void Display()
        {
            System.Console.Clear();
            System.Console.WriteLine(this._title);
            for (int i = 0; i < MenuEntries.Count; i++)
            {
                System.Console.Write(i == MenuIndex ? "> " : "  ");
                System.Console.WriteLine(MenuEntries[i].Text);
            }
        }
    }
}