﻿using System.Collections.Generic;

namespace ArcDPS_Log_Uploader.InteractiveConsole
{
    public abstract class ICMenu
    {
        internal bool IsFinalized;
        internal bool IsDone = false;
        protected readonly List<ICMenuElement> MenuEntries = new();
        protected int MenuIndex;

        public void AddEntry(string entry)
        {
            if (IsFinalized) return;
            MenuEntries.Add(new ICMenuElement {Index = MenuEntries.Count, Text = entry, IsSelected = false});
        }

        public void AddEntries(params string[] entries)
        {
            if (IsFinalized) return;
            foreach (var entry in entries)
                MenuEntries.Add(new ICMenuElement {Index = MenuEntries.Count, Text = entry, IsSelected = false});
        }

        internal virtual void UIFinalize() => IsFinalized = true;

        internal abstract ICMenuElement Select();

        internal void MoveDown()
        {
            MenuIndex++;
            if (MenuIndex >= MenuEntries.Count)
                MenuIndex = 0;
            Display();
        }

        internal void MoveUp()
        {
            MenuIndex--;
            if (MenuIndex < 0)
                MenuIndex = MenuEntries.Count - 1;
            Display();
        }
        
        internal abstract void Display();
    }
}