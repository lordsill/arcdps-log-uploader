﻿namespace ArcDPS_Log_Uploader.InteractiveConsole
{
    public class ICMenuElement
    {

        public int Index { internal init; get; }
        public string Text { internal init; get; }
        public bool IsSelected { internal set; get; }

    }
}