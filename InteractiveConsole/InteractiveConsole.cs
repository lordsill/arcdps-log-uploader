﻿using System;
using System.Collections.Generic;

namespace ArcDPS_Log_Uploader.InteractiveConsole
{
    public class InteractiveConsole
    {
        private ICMenu _menu = null;

        public ICSingleSelectionMenu CreateSingleSelectionMenu(string title)
        {
            this._menu = new ICSingleSelectionMenu(title);
            return (ICSingleSelectionMenu) this._menu;
        }

        public ICMultiSelectionMenu CreateMultiSelectionMenu(string title)
        {
            this._menu = new ICMultiSelectionMenu(title);
            return (ICMultiSelectionMenu) this._menu;
        }

        public ICMenuElement[] Draw()
        {
            if (this._menu == null) return null;
            var cursorVisible = Console.CursorVisible;
            _menu.UIFinalize();
            Console.CursorVisible = false;
            _menu.Display();
            var entries = new List<ICMenuElement>();

            do
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.DownArrow:
                        _menu.MoveDown();
                        break;
                    case ConsoleKey.UpArrow:
                        _menu.MoveUp();
                        break;
                    case ConsoleKey.Spacebar:
                    case ConsoleKey.Enter:
                        var element = _menu.Select();
                        if (element == null) break; //ICMultiSelectionMenu return null if 'Done' has been selected
                        var element2 = entries.Find(el => el.Index == element.Index);
                        if (element2 == null) entries.Add(element);
                        else entries.Remove(element2);
                        break;
                }
            } while (!_menu.IsDone);

            Console.CursorVisible = cursorVisible;
            return entries.ToArray();
        }
    }
}