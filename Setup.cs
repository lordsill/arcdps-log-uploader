﻿using System;
using System.IO;
using System.Text.Json;
using System.Text.RegularExpressions;
using ArcDPS_Log_Uploader.Entity;

namespace ArcDPS_Log_Uploader
{
    public static class Setup
    {

        internal static void Run()
        {
            Console.WriteLine("It seems like you are using this tool for the first time. All Uploads require an upload key. Please enter yours and confirm it with a press on enter");
            Console.WriteLine("To get your upload key you need a GW2 API Key with the \"account\" scope. Your key can then be requested by visiting this url: ");
            Console.WriteLine("https://tools-api.lordsill.de/gw2/logs/retrieve-key/{YOUR API KEY}");
            Console.WriteLine("So the url should look like this: https://tools-api.lordsill.de/gw2/logs/retrieve-key/ABCDEFGH-ABCD-ABCD-ABCD-ABCDEFGHIJKLMNOPQRST-ABCD-ABCD-ABCD-ABCDEFGHIJKL");
            Console.Write("Your Key: ");
            var key = Console.ReadLine() ?? "";
            var regex = new Regex(@"^[a-f\d]{8}-[a-f\d]{4}-[a-f\d]{4}-[a-f\d]{10}-[a-f\d]{6}$");
            var valid = regex.Match(key).Success;
            if (!valid)
            {
                Console.WriteLine("Invalid key entered. Please check your key and restart the application.");
                Console.ReadKey(true);
                Environment.Exit(0);
            }

            Console.Clear();
            Console.WriteLine(Program.Header);
            Console.WriteLine("The server can also upload the logs to your dps.report account. You can now enter your dps.report key if you want your logs also show up on their website or just press enter if you do not want to");
            var token = Console.ReadLine();
            var config = new Configuration()
            {
                UploadKey = key,
                DpsReportKey = token,
                Storages = Array.Empty<string>()
            };
            File.WriteAllText("config.json", JsonSerializer.Serialize(config));
            Console.Clear();
            Console.WriteLine(Program.Header);
        }

        internal static bool IsFirstStart()
        {
            return !File.Exists("config.json");
        }
    }
}