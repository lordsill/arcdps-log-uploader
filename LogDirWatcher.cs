﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;

namespace ArcDPS_Log_Uploader
{
    public static class LogDirWatcher
    {
        private static readonly string Path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                               @"\Guild Wars 2\addons\arcdps\arcdps.cbtlogs";
        
        private static readonly ConcurrentQueue<string> Queue = new();
        private static FileSystemWatcher watcher;
        internal static void Run()
        {
            Console.WriteLine("Logs Directory: " + Path);
            Console.WriteLine("Selected Collection: " + Program.Storage);
            watcher = new FileSystemWatcher()
            {
                Path = Path,
                IncludeSubdirectories = true,
                NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.FileName |
                               NotifyFilters.LastAccess,
                Filters = {"*.evtc", "*.zevtc"}
            };
            watcher.Changed += OnFileCreated;
            watcher.Created += OnFileCreated;
            watcher.Renamed += OnFileCreated;
            watcher.Error += OnWatcherError;
            watcher.InternalBufferSize *= 2;
            watcher.EnableRaisingEvents = true;

            var qThread = new Thread(() =>
            {
                while (true)
                {
                    if (!watcher.EnableRaisingEvents) watcher.EnableRaisingEvents = !watcher.EnableRaisingEvents;
                    Thread.Sleep(1000);
                    if (Queue.Count == 0) continue;
                    var top = Console.CursorTop;
                    Queue.TryDequeue(out var elem);
                    if (elem == null) continue;
                    var fName = elem[(elem.LastIndexOf(System.IO.Path.DirectorySeparatorChar) + 1)..];
                    ConsoleWrite(Status.InProgress, fName);
                    WaitForFileWriteCompletion(elem);
                    var response = Client.Upload(Program.Storage,
                        Program.Config.DpsReportKey.Length > 0 ? Program.Config.DpsReportKey : null, elem);
                    Console.CursorTop = top;
                    Console.CursorLeft = 0;
                    ConsoleWrite(response != null ? Status.Success : Status.Failed, fName);
                }
            });
            qThread.Start();
        }

        private static void OnFileCreated(object source, FileSystemEventArgs e)
        {
            Queue.Enqueue(e.FullPath);
        }

        private static void OnWatcherError(object source, ErrorEventArgs e)
        {
            Console.WriteLine($"[WatcherException] {e.GetException().Message}");
        }

        private static void WaitForFileWriteCompletion(string path)
        {
            var isWriteDone = false;
            var fileInfo = new FileInfo(path);
            do
            {
                try
                {
                    using var fileStream = fileInfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                    fileStream.Close();
                    isWriteDone = true;
                }
                catch (FileNotFoundException)
                {
                    return;
                }
                catch (IOException)
                {
                    Thread.Sleep(500);
                }
            } while (!isWriteDone);
        }

        private static void ConsoleWrite(Status status, string text)
        {
            Console.Write("[");
            switch (status)
            {
                case Status.InProgress:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write("UPLOADING");
                    break;
                case Status.Success:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("SUCCESS");
                    break;
                case Status.Failed:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("FAILED");
                    break;
                default:
                    Console.Write("UNKNOWN");
                    break;
            }
            Console.ResetColor();
            Console.WriteLine("] " + text + "            ");
        }

        private enum Status
        {
            Success,
            Failed,
            InProgress
        }
    }
}